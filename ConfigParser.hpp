
#include <string>
#include <vector>

using namespace std;

struct ConfigParser{
	string filepath;
	vector<string> commands;

	bool initConfig(string);
	string getUsage(string );
};