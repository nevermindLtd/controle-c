//#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>


#include "ConfigParser.hpp"

TEST_CASE("Config Parser initConfig","[ConfigParser][initConfig]"){
	ConfigParser parser;
	REQUIRE(parser.initConfig("file.conf"));
	REQUIRE(parser.commands[0]=="list");
	REQUIRE(parser.commands[1]=="save");
}