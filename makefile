#change your file names below
SRCtool=ConfigParser.cpp Command.cpp Display.cpp
SRCmain=main.cpp
SRCtest=ConfigParserTest.cpp CommandTest.cpp DisplayTest.cpp
EXEC=runnable
EXECtest=test

#Do not touch under this line
CXX=g++
CFLAGS=-Wall -std=c++11 -g -O0
OBJtool=$(subst .cpp,.o,$(SRCtool))
HDRtool=$(subst .cpp,.hpp,$(SRCtool))
OBJmain=$(subst .cpp,.o,$(SRCmain))
OBJtest=$(subst .cpp,.o,$(SRCtest))
	
all: main

main: tool.o main.o
	$(CXX) $(CFLAGS) -o $(EXEC) $(OBJmain) $(OBJtool)
main.o:
	$(CXX) $(CFLAGS) -c $(SRCmain) 
tool: tool.o

tool.o: $(HDRtool)
	$(CXX) $(CFLAGS) -c $(SRCtool)

test: tool.o test.o
	$(CXX) $(CFLAGS) -o $(EXECtest) $(OBJtest) $(OBJtool)
test.o:
	$(CXX) $(CFLAGS) -c $(SRCtest) 

run:
	./$(EXEC)
runtest:
	./$(EXECtest)
clean:
	rm -rf *.o
cleaner: clean
	rm -rf $(EXEC)
	rm -rf $(EXECtest)