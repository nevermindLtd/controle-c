//#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>


#include "Command.hpp"

TEST_CASE("Command initCommand","[Command][initCommand]"){
	Command command;
	string name="test name";
	string alias="test alias";
	string usage="test usage";
	vector<string> options;
	options.push_back("test option 1");
	options.push_back("test option 2");
	vector<string> conditions;
	conditions.push_back("test condition 1");
	conditions.push_back("test condition 2");

	command.initCommand( name, alias, usage, options, conditions);

	REQUIRE(command.name==name);
	REQUIRE(command.alias==alias);
	REQUIRE(command.usage==usage);
	REQUIRE(command.options==options);
	REQUIRE(command.conditions==conditions);

}