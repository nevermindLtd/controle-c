#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include "ConfigParser.hpp"

using namespace std;

bool ConfigParser::initConfig(string path)
{
    this->filepath = path;
    
    ifstream aFile(this->filepath);
    
    if (aFile) {
        string aLine;
        while(getline(aFile, aLine)){
          size_t found=aLine.find('[');
    			if (found!=string::npos)
    			{
    				size_t startCmd = aLine.find("[");
    				size_t stopCmd = aLine.find("]");
    				size_t lengthCmd=stopCmd - startCmd;
    				string cmd = aLine.substr (startCmd+1,lengthCmd-1);
    				this->commands.push_back(cmd);
    			}
        }
    }
    else {
        cerr << "Erreur: impossible d'ouvrir le fichier '"<<this->filepath<<"'."<< endl;
        return false;
    }
	return true;
}

string ConfigParser::getUsage(string command)
{
  ifstream aFile(this->filepath);
    
    if (aFile) {
        string aLine;
        while(getline(aFile, aLine)){
          //tant qu'on peut lire le fichier

          size_t found=aLine.find("["+command+"]");
          if (found!=std::string::npos)
          {
            //si on trouve la commande
            while(getline(aFile, aLine) && (aLine.find('[')==string::npos))
              /* tant qu'on peut lire une ligne ET que on ne retrouve pas une 
              nouvelle command */
            {
              if (aLine.find("usage")!=string::npos)
                //si on trouve l'usage
              {
                const char* separator ="\"";
                char *cstr = new char[aLine.length() + 1];
                strcpy(cstr, aLine.c_str());
                string usage=strtok(cstr,separator);
                usage=strtok(0,separator); //on recupère le deuxième
                return usage;
              }
            }
          }
        }
    }
    else {
        cerr << "Erreur: impossible d'ouvrir le fichier '"<<this->filepath<<"'."<< endl;
    }
    return "";
}
