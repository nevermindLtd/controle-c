#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>


#include "Display.hpp"

TEST_CASE("Display help","[Display][help]"){
	Display display;
	display.parser.initConfig("file.conf");

	REQUIRE(display.parser.getUsage("save")=="prend en compte le fichier mentionné en option");
	REQUIRE(display.parser.getUsage("list")=="liste le nombre de fichiers pris en compte");	
	REQUIRE(display.help()=="usage: prog [-h] <command> [<args>] \nles commandes à utiliser sont:\n"+display.parser.commands[0]+"	"+display.parser.getUsage(display.parser.commands[0])+"\n"+display.parser.commands[1]+"	"+display.parser.getUsage(display.parser.commands[1])+"\n");
}