#include <string>
#include <vector>

using namespace std;

struct Command{
	string name;
	string alias;
	string usage;
	vector<string> options;
	vector<string> conditions;

	void initCommand(string,string,string,vector<string>,vector<string>);
};