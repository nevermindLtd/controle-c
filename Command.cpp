#include "Command.hpp"

using namespace std;

void Command::initCommand(string name,string alias, string usage, vector<string> options,vector<string> conditions)
{
	this->name=name;
	this->alias=alias;
	this->usage=usage;
	this->options=options;
	this->conditions=conditions;
}