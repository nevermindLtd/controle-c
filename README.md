# Jérémy MAISSE #

Controle de C++ de Mr Ange ABOU.

### Récupérer le code ###

* Cloner ce répertoire à partir de son [URL](https://bitbucket.org/nevermindLtd/controle-c) dans le répertoire de votre choix sur votre ordinateur :
	
```
cd $HOME/Documents
git clone https://nevermindLtd@bitbucket.org/nevermindLtd/controle-c.git
```

* Compiler le projet: ```make```

* Exécuter les tests: ```make run```

* Nétoyer les tests:
    * effacer les .o: ```make clean```
    * effacer les .o et executable: ```make cleaner```